#!/usr/bin/env python3
import pytest
from url_rules import program_name, program_version

def test_programname():
    assert program_name() == "url_rules"
def test_programeversion():
    assert program_version() == "1.0"
