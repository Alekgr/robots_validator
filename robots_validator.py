#!/usr/bin/env python3

import requests
import sys
import requests 
import validators
import argparse
import socket
import os

def program_name()->str:
    #Returns the programs name
    return  os.path.basename(__file__).split('.')[0]

def program_version()->str:
    return  "1.0"
    

def parse_arguments()-> argparse.Namespace:
    # parse arguments 
    # return parse arguments to processs

    parser = argparse.ArgumentParser(
            prog="url_rule",
            description="Parsing webisite robots.txt",
            formatter_class=argparse.RawTextHelpFormatter
    )

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-r", "--raw", action="store_true")
    group.add_argument("-a", "--agent")

    parser.add_argument("url", help="url to parse") 
    parser.add_argument("--verbosity", help="increase verbosity", action="store_true")
    args = parser.parse_args()    

    return args

def print_domain_syntax():
    print(''' 
    Valid Domain Syntax:
    have minimum of 3 and a maximum of 63 characters
    begin with a letter or a number and end with a letter or a number;
    use the English character set and may contain letters (i.e., a-z, A-Z),numbers (i.e. 0-9) and dashes (-) or a combination of these;
    neither begin with, nor cpr144449003101 end with a dash;
    not contain a dash in the third and fourth positions (e.g. www.ab- -cd.site); and
    not include a space (e.g. www.ab cd.site).
    ''')

def getdownloaddir():
    #Returns the directory path where files are saved
    
    return os.getenv("HOME") + "/" + ".local" +"/"+ "state" + "/" + program_name()


def getfilepath(url :str):
    #get the path where  the raw robots.txt is being saved

    return  getdownloaddir()+"/"+strip_protocol(url) 


def downloaded(url: str):
    #check if the robot.txt is downloaded
   
    filepath = getfilepath(url)
    
    if os.path.exists(filepath):
        return True
    else:
        return False
    

def download_robots(url: str):
    #downloads robots.txt file from domain and
    #save it to the filepath
  
    robots_url = url+"/robots.txt"
    
    try:
        data = requests.get(robots_url) 
        data.raise_for_status()
    except requests.ConnectionError as e:
        print(f"Connection Error {e}")
        sys.exit(2) 
    except requests.HTTPError as e:
        print(F"HttpError {e}")
        sys.exit(2)
    except requests.exceptions.MissingSchema as e:
        print(F"MissingSchema {7}")
        sys.exit(7)
    #save it to a file for  processing
    else:
        filepath = getfilepath(url) 
        try:
            with open(filepath, "w") as fp:
                fp.write(data.text)
        except IOError as e:
            print("IOError")
            return False, e 

def output_robots(url: str):
    #output robots.txt raw text to console

    filepath = getfilepath(url) 
    print(filepath)
    try:
        with open(filepath, "r") as fp:
            for l in fp:
                print(l.strip())
    except IOError as e:
        print("IOError")
        return False, e 
    
def read_agent(domain: str, agent_name: str) -> list:

    enteries = []
    start = False
    filepath = getdownloaddir()+"/"+domain

    try:
        with open(filepath, "r") as fp:
            for line in fp:
                if line == "\n":
                    if start == True:
                        break
                else:
                    agent_string = line.strip()
                    agent = agent_string.split(": ")
                    if len(agent) == 1:
                        continue
                    if agent[1] == agent_name:
                        start = True
                    if start == True: 
                        enteries.append(line.strip())

    except IOError as e:
        print(f"read error {e}")

    #for i in range(len(enteries)): 
    #    print(enteries[i])
    
    return enteries 

def valid_url(url: str) -> bool:
    #check the url to make sure it is valid
    #it only checks if it has http:// or https:// in the front

    if url.startswith("http://") or url.startswith("https://"):
        return True
    else:
        return False

def strip_protocol(url: str) -> str:
    #strips the protocol from the url

    #strip http or https
    if url.startswith('http://') == True:
        d = url.replace('http://', '')
    elif url.startswith('https://') == True:
        d = url.replace('https://', '')
    else:
        d = url

    return d 
        

def validate_domain_syntax(domain):

    #check if domain is valid domain 
    #return True if domain is valid
    #return False if domain is invalid 

    
    valid=validators.domain(domain)
    if valid == True:
        return valid
    else:
        return(False)


def validate_domain_dns(domain):

    #check if url is resolvable
    try:
        socket.gethostbyname(domain) 
    except socket.gaierror as e:
        return False, e 
    else:
        return True, None 



def main():

    #get arguments
    args = parse_arguments()

    #create download directory if it doesn't exists
    if not os.path.exists(getdownloaddir()):
        print("testing")
        os.mkdir(getdownloaddir())
   

    #get the url  from argument
    url = args.url
    
    #check if url is valid
    if not valid_url(url):
        print("Inavlid URL") 
        print("Make sure you have a http:// or https:// in the front of your domain")
        exit()
    
    #get the domain for the url by striping the url
    domain = strip_protocol(url)

    #validate domain's syntax
    if validate_domain_syntax(domain) == False:
        print(f"The {domain} does not have valid syntax for a domain")
        print_domain_syntax()
        exit()

    #valid domain dns
    is_domain_valid, domain_error = validate_domain_dns(domain)
    if is_domain_valid != True:
        print(f"The domain {domain} does not a valid entry")
        print(f"{domain_error}")
        exit()
   
    #download  
    if not downloaded(url):
        print("Downloading ..")
        download_robots(url) 
    
    #get the agent name or print raw
    if args.agent:
        agent = read_agent(domain, args.agent)
        for i in agent:
            print(i)
    else:
        output_robots(url)
         
     
        
if __name__ == "__main__":
    main()


